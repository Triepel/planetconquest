package planetConquest;

public class Game {

	private Display display;
	private int width , height;
	private int fps;
	
	
	public void setFps(int fps) {
		this.fps = fps;
	}

	public Game( int width, int height) {
		this.width = width;
		this.height = height;
		display = new Display(width, height);  
	}
	
	public void update() {
		
	}
	public void render() {
		display.render(this);
		display.setFps(fps);
	
		
	}
}
