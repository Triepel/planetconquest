package planetConquest;

public class Gameloop implements Runnable{

	private Game game;
	
	
	private boolean running; 
	private final double updateRate = 1d/60.0d; 
	
	private long nextStatTime;
	private int fps, ups;
	
	public Gameloop (Game game) {
		this.game = game; 
	}
	
	@Override
	public void run() {
		
		running = true;
		nextStatTime = System.currentTimeMillis() + 1000 ;
		
		final int MaxFPS = 144;
		final int MaxUPS = 60;
		
		final double upsOptimalTime = 1000000000 / MaxUPS;
		final double fpsOptimalTime = 1000000000 / MaxFPS;
		
		double upsDeltaTime = 0, fpsDeltaTime = 0;
		long startTime = System.nanoTime();
		
		while(running){
			long currentTime = System.nanoTime();
			upsDeltaTime += (currentTime - startTime);
			fpsDeltaTime += (currentTime - startTime);
			startTime = currentTime;
			
			if(upsDeltaTime >= upsOptimalTime) {
				update();
				
				
				upsDeltaTime -= upsOptimalTime; 				
			}
			if(fpsDeltaTime >= fpsOptimalTime) {
				render();
				
				fpsDeltaTime -= fpsOptimalTime; 				
			}
			printStats();
			
		}
	}

	private void printStats() {
		if(System.currentTimeMillis()> nextStatTime) {
		System.out.println("FPS: "+ fps +" UPS: "+ ups);
		game.setFps(fps);
		fps = 0;
		ups = 0;
		
		nextStatTime = System.currentTimeMillis() +1000;
		
		}
	}
	
	private String getfps() {
		return "" + fps;
	}
	
	

	private void render() {
		game.render();
		fps++;
		
		
	}

	private void update() {
		game.update();
		ups++;
		
	}
	
	
	

	
	
}
