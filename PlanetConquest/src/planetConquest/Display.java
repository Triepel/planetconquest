package planetConquest;
import java.awt.*;
import java.awt.image.BufferStrategy;
import javax.swing.*;


public class Display extends JFrame {

	private Canvas canvas;
	private int fps;
	


	//Create the frame//
	 
	




	public Display (int width, int height) {
		setTitle("PlanetConquest");
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		setResizable(false); 
			
		
		canvas = new Canvas();
		canvas.setPreferredSize(new Dimension(width, height));
		canvas.setFocusable(false);
		getContentPane().add(canvas);
		pack();
		

		
		setLocationRelativeTo(null);
		setVisible(true);
		
		canvas.createBufferStrategy(3);
		
		
		
	}
	
	

	
	public void render(Game game) {
		BufferStrategy bufferStrategy = canvas.getBufferStrategy();
		Graphics graphics =bufferStrategy.getDrawGraphics();
		
		graphics.setColor(Color.BLACK);
		graphics.fillRect(0, 0, canvas.getWidth(), canvas.getHeight());
		graphics.setColor(Color.YELLOW);
		graphics.setFont(new Font ("Arial",Font.PLAIN,15 )); 
		graphics.drawString("FPS:"+ fps, 0, 10);
		
		
		graphics.dispose();
		bufferStrategy.show();
		
		
		
		
	}
	
	public void setFps(int fps) {
		this.fps = fps;
	}
}
